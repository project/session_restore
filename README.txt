
  The session restore module allows sessions (which are stored in a separate table) to be
restored upon login. This module can be used along with other session related modules
such as "Auto logout" and "Session limit".

AUTHOR
------
  Karthik Kumar / Zen [ http://drupal.org/user/21209 ].

